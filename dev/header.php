<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' : '; } ?><?php bloginfo('name'); ?></title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <?php wp_head(); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/js/header.js"></script>
</head>
<body <?php body_class(); ?>>
    <!-- header -->
      <header class="js-header">

        <!-- container -->
        <div class="container">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="header-logo" src="<?php echo get_template_directory_uri(); ?>/img/headerLogo.png" alt="Veoo"/></a>

          <!-- desktop-nav -->
          <nav class="desktop" role="navigation">
            <ul>
                <li>
                  <input class="search-box" type="text">
                </li>
              <?php wp_nav_menu_no_ul(); ?>
              <li class="contact-button">Sign Up</li>
              </ul>
          </nav>
          <!-- /desktop-nav -->

          <!-- mobile-nav -->
          <nav class="mobile" role="navigation">
            <div class="js-menu-button menu-button">
              <img class="active js-menu-image-hamburger" src="<?php echo get_template_directory_uri(); ?>/img/menu.png" alt="Menu"/>
              <img class="js-menu-image-cross" src="<?php echo get_template_directory_uri(); ?>/img/cross.png" alt="Close Menu" />
            </div>
            <div class="contact-button">Sign Up</div>
            <input class="search-box" type="text">
          </nav>
          <!-- /mobile-nav -->

        </div>
        <!-- /container -->

          <!-- mobile-menu -->
          <nav class="js-mobile-menu mobile-menu">
            <ul>
              <?php wp_nav_menu_no_ul(); ?>
            </ul>
          </nav>
          <!-- /mobile-menu -->

      </header>
      <!-- /header -->
