// Document ready.
$(document).ready(function() {
  'use strict';

  /**
   * Nav scroll event.
   */
  var navScrollEvent = function() {
    var scrollTop = $(window).scrollTop();
    hideMobileMenuOnMobile();
    var header = $('.js-header');

    if (scrollTop <= 5) {
      header.removeClass('solid-background');
      header.addClass('transparent-background');
    }
    else {
      header.removeClass('transparent-background');
      header.addClass('solid-background');
    }
  };

  /**
   * Toggle nav menu button.
   */
  var toggleNavMenu = function() {
    $('.js-mobile-menu').fadeToggle('fast');
    $('.js-menu-image-hamburger').toggleClass('active');
    $('.js-menu-image-cross').toggleClass('active');
  };

  /**
   * Hide mobile nav on desktop.
   */
  var hideMobileNavOnDesktop = function() {
    if ($(window).width() > 480) {
      $('.mobile-menu').hide();
      $('.js-menu-image-hamburger').addClass('active');
      $('.js-menu-image-cross').removeClass('active');
    }
  };

  /**
   * if else where in the page is click apart from mobile menu close mobile menu
  */
  var hideMobileMenuOnMobile = function() {
    var container = $('.mobile-menu');
    var menuButton = $('.js-menu-image-hamburger');
    if (!container.is(event.target) && container.has(event.target).length === 0 && !menuButton.is(event.target)) {
      container.fadeOut('fast');
      menuButton.addClass('active');
      $('.js-menu-image-cross').removeClass('active');
    }
  };

  /**
   * Smooth scroll to divs.
   */
  var smoothScrollToDiv = function() {
    if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      hideMobileMenuOnMobile();
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  };

  // Scroll events
  $(window).on('scroll', navScrollEvent);

  // Click events
  $('.js-menu-button').on('click', toggleNavMenu);
  $('a[href*=#]:not([href=#])').on('click', smoothScrollToDiv);
  $('html').on('click', hideMobileMenuOnMobile);

  // Resize events
  $(window).on('resize', hideMobileNavOnDesktop);
});
