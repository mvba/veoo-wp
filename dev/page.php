<?php
/*
 * Template Name: General page template
 * Description: Template for  most pages on the site
 * Author: Brett Dorrans & Chris Feetham
 */
?>
<?php get_header(); ?>
<!-- content -->
<section class="general-page constrained">
    <div class="container">
        <div class="row">
          <div class="full general-page-header">
            <h1><?php echo get_the_title(); ?></h1>
            <nav>
              <ul>
                <li><a href="index.html">Europe</a></li>
                <li><a href="index.html">America</a></li>
                <li><a href="index.html">Asia</a></li>
                <li><a href="index.html">Australia</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="row">
          <div class="full general-page-content">
            <p>Veoo enables your business to deliver and bill for mobile services and content around the world whilst simplifying commercial, technical and regulatory complexities. We specialise in supporting our clients with country-by-country expertise and our platform allows you to move into each new territory with the safety of our market knowledge.</p>

            <p>Veoo offers both free to user messaging and mobile billing. We help you reach out to your customer base through our various marketing tools to maximise your revenue opportunities. In addition to traditional billing mechanisms, Veoo offers its unique innovation in iPayer; designed specifically to enable in-application payment via mobile billing. Our aim is to grow your business via mobile, allowing you to focus on what you do best.</p>

            <p>Communication is made simple by our web user interface designed by people from varying backgrounds within the mobile messaging industry. Our team has knowledge of aggregation as well as direct to consumer mobile services. This has helped us to create a service to form a new type of aggregation and billing partner where the client can control their whole business from a single website, through Veoo’s user interface. Veoo will allow for general account management, from sign up through to keyword ordering, reporting and CRM. Veoo also includes a channel designed to work with both client and regulatory bodies within each market we serve.</p>
          </div>
        </div>
        <div class="row">
          <div class="full general-page-content section-image-container">
             <img class="section-image" src="<?php echo get_template_directory_uri(); ?>/img/aboutImage.jpg">
             <div class="gradient">
             </div>
          </div>
        </div>
    </div>
</section>
<!--/ content -->
 <?php get_footer(); ?>
