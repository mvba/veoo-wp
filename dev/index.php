<?php get_header(); ?>

 <!-- hero -->
    <section class="hero view-port-height">
      <div class="container">
        <div class="row">
          <div class="hero-content view-port-height vertical-align-middle">
            <h2>premium, cloud &amp; bulk sms solutions</h2>
            <p>
              <a href="#hello" class="contact-button">say hello</a>
            </p>
             <a href="#hello" ><img src="<?php echo get_template_directory_uri(); ?>/img/arrow.png" class="arrow" /></a>
          </div>
        </div>
      </div>
    </section>
    <!-- /hero -->

    <!-- landing-content -->
    <section class="landing-content">
      <div class="container center-content-horizontal">

        <!-- landing-content-cta -->
        <div class="row vertical-align-middle">
          <div class="full">
            <h2>What can we do for your business?</h2>
          </div>
        </div>
        <!-- /landing-content-cta -->

        <div class="row">
          <!-- column -->
          <div class="third">
            <img src="<?php echo get_template_directory_uri(); ?>/img/0079_Veoo_SMS_Icon.svg" class="icon"/>
            <h3>SMS<br/>Gateways</h3>
            <a class="button" href="#">More</a>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun</p>
          </div>
          <!-- /column -->

          <!-- column -->
          <div class="third">
            <img src="<?php echo get_template_directory_uri(); ?>/img/0079_Veoo_MobilePayments_Icon.svg" class="icon"/>
            <h3>Mobile<br/>Payments</h3>
            <a class="button" href="#">More</a>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun</p>
          </div>
          <!-- /column -->

          <!-- column -->
          <div class="third">
            <img src="<?php echo get_template_directory_uri(); ?>/img/0079_Veoo_Marketing_Icon.svg" class="icon"/>
            <h3>Mobile<br/>Marketing</h3>
            <a class="button" href="#">More</a>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun</p>
          </div>
          <!-- /column -->
        </div>

      </div>

      <!-- contact-form -->
      <div class="row contact-form" id="hello">
        <div class="full">

          <div class="form-main">
            <div class="form-div">
              <form class="form">
                <h2 class="center-content-horizontal">Hello</h2>
                <p class="name">
                  <input name="name" type="text" class="name-field validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Name" />
                </p>
                <p class="email">
                  <input name="email" type="text" class="email-field validate[required,custom[email]] feedback-input"  placeholder="Email" />
                </p>
                <p class="text">
                  <textarea name="text" class="comment-field validate[required,length[6,300]] feedback-input" placeholder="Comment"></textarea>
                </p>
                <p class="submit">
                  <input type="submit" value="SEND" class="button-blue"/>
                </p>
              </form>
            </div>
          </div>

        </div>
      </div>
       <!-- /contact-form -->

<?php get_footer(); ?>
