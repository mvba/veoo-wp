/**
 * Gruntfile.js for wp-boilerplate.
 *
 * Available grunt commands:
 *
 * grunt dev — Compile SCSS to CSS, concatenate and validate JS
 * grunt build — Create minified assets that are used on non-development environments
 */

module.exports = function(grunt) {
    'use strict';

    var pkg = grunt.file.readJSON('package.json');

    // Directories
    var dir = {
        dev: 'dev',                                     // Development
        dist: 'dist',                                   // Production
        scss: 'dev/scss',                               // SCSS
        js: 'dev/js',                                   // Javascript
        tmp: 'dev/.tmp',                                // Temp
        theme: pkg.config.wp_theme_dir + '/' + pkg.name // WordPress theme
    };

    // List of JS files that go in the <head> tag
    var headerJS = [
    ];

    // List of JS files that go before the </body> tag
    var footerJS = [
    ];

    // Load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // Show elapsed time
    require('time-grunt')(grunt);

    // Tasks
    grunt.initConfig({
        dir: dir,
        pkg: pkg,
        banner: '/*\n' +
            '\tTheme Name: <%= pkg.config.wp_theme_name %>\n' +
            '\tTheme URI: <%= pkg.homepage %>\n' +
            '\tAuthor: <%= pkg.author %>\n' +
            '\tAuthor URI: <%= pkg.homepage %>\n' +
            '\tDescription: <%= pkg.description %>\n' +
            '\tVersion: <%= pkg.version %>\n' +
            '\tText Domain: <%= pkg.name %>\n' +
            '\tTags:\n' +
            '*/\n',

        // Clean directories
        clean: {
            options: {
                force: true
            },
            dev: {
                src: ['<%= dir.tmp %>', '<%= dir.theme %>']
            },
            build: {
                src: ['<%= dir.tmp %>', '<%= dir.dist %>', '<%= dir.theme %>']
            }
        },

        // Version bumping
        bumpup: {
            files: ['package.json']
        },

        // Lint javascript
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                '<%= dir.js %>/**/*.js',
                '!<%= dir.js %>/vendor/**/*'
            ]
        },

        // Lint SCSS
        scsslint: {
            all: ['<%= dir.scss %>/**/*.scss', '!<%= dir.scss %>/vendor/**/*']
        },

        // Compile SCSS to CSS
        sass: {
            all: {
                options: {
                    sourcemap: 'none',
                    banner: '<%= banner %>'
                },
                files: {
                    '<%= dir.tmp %>/style.css': '<%= dir.scss %>/style.scss'
                }
            }
        },

        // Concat javascript files
        concat: {
            options: {
                separator: ';',
                stripBanners: true
            },
            all: {
                files: {
                    '<%= dir.tmp %>/header.js': [headerJS],
                    '<%= dir.tmp %>/footer.js': [footerJS],
                    '<%= dir.tmp %>/app.js': ['<%= dir.js %>/*.js', '!<%= dir.js %>/vendor/**/*.js']
                }
            }
        },

        // Minify/uglify javascript
        uglify: {
            build: {
                options: {
                    mangle: false,
                    sourcemap: false,
                    compress: false
                },
                files: {
                    'dist/js/header.js': '<%= dir.tmp %>/header.js',
                    'dist/js/footer.js': '<%= dir.tmp %>/footer.js',
                    'dist/js/app.js': '<%= dir.tmp %>/app.js'
                }
            }
        },

        // Minify CSS
        cssmin: {
            build: {
                options: {
                    banner: '<%= banner %>'
                },
                files: {
                    '<%= dir.dist %>/style.css': '<%= dir.tmp %>/style.css'
                }
            }
        },

        // Replace package info in .php files with correct data
        replace: {
            dev: {
                src: ['<%= dir.theme %>/*.php'],
                overwrite: true,
                replacements: [{
                    from: '{{package}}',
                    to: '<%= pkg.name %>'
                }, {
                    from: '{{version}}',
                    to: '<%= pkg.version %>'
                }]
            },
            build: {
                src: ['<%= dir.theme %>/*.php', '<%= dir.dist %>/*.php'],
                overwrite: true,
                replacements: [{
                    from: '{{package}}',
                    to: '<%= pkg.name %>'
                }, {
                    from: '{{version}}',
                    to: '<%= pkg.version %>'
                }]
            }
        },

        // Copy files over to WordPress theme directory and dist directory
        copy: {
            dev: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.tmp %>',
                    src: ['style.css'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.tmp %>',
                    src: ['*.js'],
                    dest: '<%= dir.theme %>/js'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/js',
                    src: ['*.htc'],
                    dest: '<%= dir.theme %>/js'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['screenshot.png'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['**'],
                    dest: '<%= dir.theme %>/img'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/functions',
                    src: ['**'],
                    dest: '<%= dir.theme %>/functions'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['**/*.php'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['**'],
                    dest: '<%= dir.theme %>/img'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['*.png', '*.xml'],
                    dest: '<%= dir.theme %>'
                }]
            },
            build: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['screenshot.png'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['**'],
                    dest: '<%= dir.theme %>/img'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/functions',
                    src: ['**'],
                    dest: '<%= dir.theme %>/functions'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['**/*.php'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['screenshot.png'],
                    dest: '<%= dir.dist %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['**'],
                    dest: '<%= dir.dist %>/img'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/functions',
                    src: ['*'],
                    dest: '<%= dir.dist %>/functions'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['**/*.php'],
                    dest: '<%= dir.dist %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dist %>',
                    src: ['style.css'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dist %>/js',
                    src: ['*.js'],
                    dest: '<%= dir.theme %>/js'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/js',
                    src: ['*.htc'],
                    dest: '<%= dir.theme %>/js'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['**'],
                    dest: '<%= dir.theme %>/img'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['*.png', '*.xml'],
                    dest: '<%= dir.theme %>'
                }]
            },
            scss: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.tmp %>',
                    src: ['style.css'],
                    dest: '<%= dir.theme %>'
                }]
            },
            js: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.tmp %>',
                    src: ['*.js'],
                    dest: '<%= dir.theme %>/js'
                }]
            },
            php: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.dev %>/functions',
                    src: ['**'],
                    dest: '<%= dir.theme %>/functions'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['**/*.php'],
                    dest: '<%= dir.theme %>'
                }]
            }
            img: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.dev %>',
                    src: ['screenshot.png'],
                    dest: '<%= dir.theme %>'
                }, {
                    expand: true,
                    cwd: '<%= dir.dev %>/img',
                    src: ['*'],
                    dest: '<%= dir.theme %>/img'
                }]
            }
        },

        // Watch for changes to files
        watch: {
          options: {
            livereload: true
          },
            scss: {
                files: [
                    '<%= dir.scss %>/*.scss',
                    '<%= dir.scss %>/**/*.scss'
                ],
                tasks: [
                    'bumpup:patch',
                    'scsslint',
                    'sass',
                    'copy:scss'
                ]
            },
            js: {
                files: [
                    '<%= dir.js %>/*.js',
                    '<%= dir.js %>/**/*.js'
                ],
                tasks: [
                    'bumpup:patch',
                    'jshint',
                    'concat',
                    'copy:js'
                ]
            },
            gruntfile: {
                files: [
                    'Gruntfile.js'
                ],
                options: {
                    reload: true
                }
            },
            php: {
                files: [
                    '<%= dir.dev %>/**/*.php',
                    '<%= dir.dev %>/functions/**/*.php',
                ],
                tasks: [
                    'bumpup:patch',
                    'copy:php',
                    'replace'
                ]
            },
            img: {
                files: [
                    '<%= dir.dev %>/img/*.*',
                    '<%= dir.dev %>/screenshot.png'
                ],
                tasks: [
                    'copy:img'
                ]
            }
        }
    });

    // Register tasks
    grunt.registerTask('default', [
        'dev'
    ]);
    grunt.registerTask('dev', [
        'clean:dev',
        'bumpup:prerelease',
        'jshint',
        'scsslint',
        'sass',
        'concat',
        'copy:dev',
        'replace:dev',
        'watch'
    ]);
    grunt.registerTask('build', [
        'clean:build',
        'bumpup:patch',
        'jshint',
        'scsslint',
        'sass',
        'concat',
        'uglify',
        'cssmin',
        'copy:build',
        'replace:build'
    ]);
};
